-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2015 at 12:21 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quantity_manager`
--

-- --------------------------------------------------------

--
-- Table structure for table `table_qty`
--

CREATE TABLE IF NOT EXISTS `table_qty` (
`id` int(11) NOT NULL,
  `day` text NOT NULL,
  `date` date NOT NULL,
  `manager` text NOT NULL,
  `quantity` int(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_qty`
--

INSERT INTO `table_qty` (`id`, `day`, `date`, `manager`, `quantity`) VALUES
(1, 'Monday', '2015-12-29', 'Mazhar', 455),
(2, 'Tuesday', '2015-12-12', 'Rahad', 520),
(3, 'Wednesday', '2015-12-12', 'kamal', 520),
(4, '', '2015-11-12', 'John', 855);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `table_qty`
--
ALTER TABLE `table_qty`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `table_qty`
--
ALTER TABLE `table_qty`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
